package com.zenika.academy.woodblockML.data;

import com.zenika.academy.woodblockML.domain.Command;
import org.springframework.data.repository.CrudRepository;

public interface CommandRepository extends CrudRepository<Command, Integer> {
}
