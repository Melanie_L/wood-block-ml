import { UserList, CreateUser, CreateCommand, CommandList } from "./pages";
import VueRouter from "vue-router";

export const routes = [
  {
    path: "/userList",
    component: UserList,
    label: "Users list"
  },
  {
      path:"/createUser",
      component: CreateUser,
      label: "New user"
  },
  {
      path:"/newCommand",
      component: CreateCommand,
      label: "New command"
  },
  {
      path:"/commandList",
      component: CommandList,
      label: "Command list"
  }
];

const router = new VueRouter({
  routes
});

export default router;
