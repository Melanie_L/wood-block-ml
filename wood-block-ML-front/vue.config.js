const path = 'path';
module.exports = {
   //ici on dit qu'on surchage le port
    devServer: {
        //ici on def le proxy 
        proxy: {
            '^/api': {
                //si on a un /api on renvoie à l'adresse mis dans le target
                //on écrit pu dans l'adresse le localhost et le port
                target: 'http://localhost:8080'
            }
        },
        port: 8081
    }
};