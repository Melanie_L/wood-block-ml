package com.zenika.academy.woodblockML.data;

import com.zenika.academy.woodblockML.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository <User, Integer> {
}
