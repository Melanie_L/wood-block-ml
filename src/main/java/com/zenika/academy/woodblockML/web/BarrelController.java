package com.zenika.academy.woodblockML.web;

import com.zenika.academy.woodblockML.data.BarrelRepository;
import com.zenika.academy.woodblockML.domain.Barrel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Optional;

@RestController
@RequestMapping(path = "/barrels",
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)

@Slf4j
public class BarrelController {

    private final BarrelRepository barrelRepository;

    public BarrelController(BarrelRepository barrelRepository) {
        this.barrelRepository = barrelRepository;

    }


    @PostMapping
    @Transactional
    public Barrel addBarrel(@RequestBody Barrel barrel) {

        barrelRepository.save(barrel);
        return barrel;
    }

    @GetMapping
    public ResponseEntity<Iterable<Barrel>> readAllBarrel() {
        return ResponseEntity.ok(barrelRepository.findAll());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Barrel> readBarrel(@PathVariable int id) {
        Optional<Barrel> optionalBarrel = barrelRepository.findById(id);
        if (optionalBarrel.isPresent()) {
            return ResponseEntity.ok(optionalBarrel.get());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @DeleteMapping(path = "/{id}")
    @Transactional
    public ResponseEntity<Barrel> deleteBarrel(@PathVariable int id) {
        Optional<Barrel> barrel = barrelRepository.findById(id);

        return barrel.map(c -> {
            barrelRepository.deleteById(id);
            return ResponseEntity.ok(c);
        }).orElse(ResponseEntity.notFound().build());
    }
}
