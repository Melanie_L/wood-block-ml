package com.zenika.academy.woodblockML.domain.woodBlockParam;

public enum WBFinishing {

    SATIN(0.50),  //Finishing price for 1m2
    MATT(0.30),
    BRIGHT(0.90),
    BRUTE (0.0);

    WBFinishing(double v) {
    }
}
