package com.zenika.academy.woodblockML.domain;


import com.zenika.academy.woodblockML.domain.commandState.State;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Command {

    @Id
    @GeneratedValue
    private int id;

    private State state;

    private int totalPrice;

    private int blockNumber;

    private int budget;

}
