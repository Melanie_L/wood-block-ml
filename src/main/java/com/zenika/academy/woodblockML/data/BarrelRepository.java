package com.zenika.academy.woodblockML.data;

import com.zenika.academy.woodblockML.domain.Barrel;
import org.springframework.data.repository.CrudRepository;

public interface BarrelRepository extends CrudRepository<Barrel, Integer> {
}
