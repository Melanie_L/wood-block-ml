package com.zenika.academy.woodblockML.data;

import com.zenika.academy.woodblockML.domain.WoodBlock;
import org.springframework.data.repository.CrudRepository;

public interface WoodBlockRepository extends CrudRepository<WoodBlock, Integer> {

}
