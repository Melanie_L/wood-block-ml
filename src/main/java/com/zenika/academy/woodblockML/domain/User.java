package com.zenika.academy.woodblockML.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "woodblock_user")
public class User {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String firstname;

    @NotNull
    private String lastname;

    @NotNull
    private String address;

    @NotNull
    @UniqueElements
    private String mail;

    @NotNull
    private String password;

    @NotNull
    private String phone;

   /* @NotNull
    @ManyToOne (optional = false)
    private Command command;*/
}
