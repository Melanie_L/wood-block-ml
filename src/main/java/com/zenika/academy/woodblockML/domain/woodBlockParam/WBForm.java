package com.zenika.academy.woodblockML.domain.woodBlockParam;

public enum WBForm {
    CYLINDER,
    SQUARE,
    RECTANGLE,
    HEXAGONAL,
    TRIANGLE;

    private String wbForm = "";

    WBForm() {

    }


    @Override
    public String toString() {
        return "WBForm{" +
                "wbForm='" + wbForm + '\'' +
                '}';
    }
}
