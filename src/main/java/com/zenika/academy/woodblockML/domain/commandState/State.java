package com.zenika.academy.woodblockML.domain.commandState;

public enum State {
    WAITING_VALIDATION,
    WAITING_PAIEMENT,
    BUILDING,
    SEND,
    RECEIVED;

    State() {
    }
}
