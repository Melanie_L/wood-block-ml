package com.zenika.academy.woodblockML.web;

import com.zenika.academy.woodblockML.data.CommandRepository;
import com.zenika.academy.woodblockML.domain.Command;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Optional;

@RestController
@RequestMapping(path = "/commands",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE)

@Slf4j
public class CommandController {

    private final CommandRepository commandRepository;

    public CommandController(CommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @PostMapping
    @Transactional
    public Command addCommand(@RequestBody Command command) {

        commandRepository.save(command);
        return command;
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Command> readCommand(@PathVariable int id) {
        Optional<Command> optionalCommand = commandRepository.findById(id);
        if (optionalCommand.isPresent()) {
            return ResponseEntity.ok(optionalCommand.get());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping()
    public ResponseEntity<Iterable<Command>> readAllCommand() {
        return ResponseEntity.ok(commandRepository.findAll());
    }

    @DeleteMapping(path = "/{id}")
    @Transactional
    public ResponseEntity<Command> deleteCommand(@PathVariable int id) {
        Optional<Command> command = commandRepository.findById(id);

        return command.map(c -> {
            commandRepository.deleteById(id);
            return ResponseEntity.ok(c);
        }).orElse(ResponseEntity.notFound().build());
    }
}
