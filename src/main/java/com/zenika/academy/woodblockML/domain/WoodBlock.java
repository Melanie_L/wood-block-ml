package com.zenika.academy.woodblockML.domain;

import com.zenika.academy.woodblockML.domain.woodBlockParam.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class WoodBlock {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private WBForm form;

    @NotNull
    private WBHeight height;

    @NotNull
    private WBFinishing finishing;

    private WBColor color;

    private WBWoodType woodType;
}
