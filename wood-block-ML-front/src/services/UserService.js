const API = "/api/users";

const defaultHeaders = new Headers();
defaultHeaders.append("Content-Type", "application/json");

const parseJSON = response => response.json();


export const getAllUsers = () =>
  fetch(API, {
    headers: defaultHeaders
  }).then(parseJSON);



export const saveUser = user =>
    fetch(API, {
        headers: defaultHeaders,
        method: "POST",
        body: JSON.stringify(user)
    }).then(parseJSON);