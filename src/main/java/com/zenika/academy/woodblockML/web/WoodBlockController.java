package com.zenika.academy.woodblockML.web;


import com.zenika.academy.woodblockML.data.WoodBlockRepository;
import com.zenika.academy.woodblockML.domain.WoodBlock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Optional;

@RestController
@RequestMapping(path = "/woodBlock",
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)

@Slf4j
public class WoodBlockController {

    private final WoodBlockRepository woodBlockRepository;

    public WoodBlockController(WoodBlockRepository woodBlockRepository) {
        this.woodBlockRepository = woodBlockRepository;
    }

    @PostMapping
    @Transactional
    public void addBlock(@RequestBody WoodBlock woodBlock) {

        woodBlockRepository.save(woodBlock);
    }

    @GetMapping
    public ResponseEntity<Iterable<WoodBlock>> readBlock() {
        return ResponseEntity.ok(woodBlockRepository.findAll());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Optional<WoodBlock>> readBlock(@PathVariable int id) {
        return ResponseEntity.ok(woodBlockRepository.findById(id));
    }

    @DeleteMapping(path = "/{id}")
    @Transactional
    public ResponseEntity<WoodBlock> deleteBlock(@PathVariable int id) {
        Optional<WoodBlock> woodBlock = woodBlockRepository.findById(id);

        return woodBlock.map(c -> {
            woodBlockRepository.deleteById(id);
            return ResponseEntity.ok(c);
        }).orElse(ResponseEntity.notFound().build());
    }
}






