package com.zenika.academy.woodblockML.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Barrel {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private int price;

    @NotNull
    private int blockQuantity;

    @OneToMany (targetEntity = WoodBlock.class, cascade = CascadeType.ALL)
    private List<WoodBlock> blocks;
}
