package com.zenika.academy.woodblockML.domain.woodBlockParam;

public enum WBWoodType {

    BEECH(300), //Beech cost 300€ for 1m3
    MAHOGANY(40000),
    OAK(900),
    PIN(200);

    WBWoodType(int i) {

    }
}
