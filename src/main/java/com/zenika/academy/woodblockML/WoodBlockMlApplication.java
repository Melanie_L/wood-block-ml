package com.zenika.academy.woodblockML;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WoodBlockMlApplication {

	public static void main(String[] args) {
		SpringApplication.run(WoodBlockMlApplication.class, args);
	}

}
