package com.zenika.academy.woodblockML.domain.woodBlockParam;

public enum WBColor {
    BLUE("blue"),
    WHITE ("white"),
    RED("red"),
    YELLOW("yellow"),
    ORANGE("orange"),
    GREEN("green"),
    BLACK("black");

    WBColor(String color) {
    }
}
