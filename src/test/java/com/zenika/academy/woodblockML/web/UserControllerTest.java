package com.zenika.academy.woodblockML.web;

import com.zenika.academy.woodblockML.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "dev")
@SpringBootTest()
public class UserControllerTest {


    @Test
    public void doubleUserTest() {

        User user1 = User.builder()
                .firstname("Pauline")
                .lastname("Jacquemart")
                .mail("pauline@mail")
                .address("15 rue du moulin")
                .phone("02 20 20")
                .password("pauline")
                .build();


        User user2 = User.builder()
                .firstname("Pauline")
                .lastname("Jacquemart")
                .mail("pauline@mail")
                .address("15 rue du moulin")
                .phone("02 20 20")
                .password("pauline")
                .build();

        //then
        //the application allow to create two identical users
        assertThat(user2).isEqualTo(user1);

    }
}