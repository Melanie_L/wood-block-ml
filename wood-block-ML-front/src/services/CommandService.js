const API = "/api/commands";

const defaultHeaders = new Headers();
defaultHeaders.append("Content-Type", "application/json");

const parseJSON = response => response.json();

export const getAllCommands = () =>
    fetch(API, {
        headers: defaultHeaders
    }).then(parseJSON);


export const saveCommand = command =>
    fetch(API, {
        headers: defaultHeaders,
        method: "POST",
        body: JSON.stringify(command)
    }).then(parseJSON);


