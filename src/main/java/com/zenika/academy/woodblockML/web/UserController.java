package com.zenika.academy.woodblockML.web;

import com.zenika.academy.woodblockML.data.UserRepository;
import com.zenika.academy.woodblockML.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Optional;

@RestController
@RequestMapping(path = "/users",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE)


@Slf4j
public class UserController {

    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @PostMapping
    @Transactional
    public User addUser(@RequestBody User user) {

        userRepository.save(user);
        return user;

    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<User> readUser(@PathVariable int id) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            return ResponseEntity.ok(optionalUser.get());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping()
    public ResponseEntity<Iterable<User>> readAllUser() {
        return ResponseEntity.ok(userRepository.findAll());
    }

    @DeleteMapping(path = "/{id}")
    @Transactional
    public ResponseEntity<User> deleteUser(@PathVariable int id) {
        Optional<User> user = userRepository.findById(id);

        return user.map(c -> {
            userRepository.deleteById(id);
            return ResponseEntity.ok(c);
        }).orElse(ResponseEntity.notFound().build());
    }
}

